<?php

use App\Http\Controllers\authController;
use App\Http\Controllers\transactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('dashboard', [authController::class, 'dashboard'])->name('dashboard');

//auth controller
Route::get('login', [authController::class, 'index'])->name('login');
Route::post('custom-login', [authController::class, 'customLogin'])->name('login.custom');
Route::get('registration', [authController::class, 'registration'])->name('register-user');
Route::post('custom-registration', [authController::class, 'customRegistration'])->name('register.custom');
Route::get('signout', [authController::class, 'signOut'])->name('signout');

//transaction controller
Route::get('transaction', [transactionController::class, 'index'])->name('transaction');
Route::get('transaction/create', [transactionController::class, 'add'])->name('create-transaction');
Route::post('transaction/store', [transactionController::class, 'store'])->name('create-transaction-store');

Route::get('transaction/edit/{id}', [transactionController::class, 'show'])->name('show-transaction');
Route::post('transaction/update/{id}', [transactionController::class, 'edit'])->name('edit-transaction');

Route::post('transaction/delete/{id}', [transactionController::class, 'delete'])->name('delete-transaction');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
