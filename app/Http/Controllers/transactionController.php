<?php

namespace App\Http\Controllers;

use App\DataTables\transactionsDataTable;
use App\Models\transaction;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class transactionController extends Controller
{
    function generateRandomString($length = 6)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $randomString = '';

        $max = strlen($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $max)];
        }

        return strtoupper($randomString);
    }


    // list index
    public function index(transactionsDataTable $dataTable)
    {
        if (Auth::check()) {
            $totalAmount = DB::select("select sum(amount) amt from (
                select case when transaction_type = 'D' then amount when transaction_type = 'C' then amount * -1 end as amount  from transactions t
                ) d");

            $amount = $totalAmount[0]->amt;
            return $dataTable->render('transactions.index', compact('amount'));
        }
        return view('dashboard');
    }

    // add transaction
    public function add()
    {
        if (Auth::check()) {
            return view('transactions.create');
        }
        return view('dashboard');
    }

    public function store(Request $request)
    {
        if (Auth::check()) {
            $code = $request->code . transactionController::generateRandomString();
            $amount = (float) str_replace(',', '', $request->amount);

            $transaction = new transaction;
            $transaction->code = $code;
            $transaction->amount = $amount;
            $transaction->transaction_type = $request->transaction_type;
            $transaction->user_id = Auth::user()->id;
            $transaction->notes = $request->notes;
            // if debit upload file
            if ($request->transaction_type == 'D') {
                if ($request->hasFile('file')) {
                    $file = $request->file('file');
                    $extension = $file->getClientOriginalExtension();
                    $filename = time() . '.' . $extension;
                    $file->move('uploads/transaction/', $filename);
                    $transaction->upload = $filename;
                }
            } else {
                $transaction->upload = '';
            }

            $transaction->save();
            return redirect('transaction')->with('status', 'data sudah berhasil di simpan');
        }
        return redirect('dashboard');
    }

    // edit transaction
    public function show($id)
    {
        if (Auth::check()) {
            $transaction = Transaction::find($id);
            // print_r($transaction);
            // die();
            return view('transactions.edit', compact('transaction'));
        }
        return view('dashboard');
    }

    public function edit(Request $request, $id)
    {
        if (Auth::check()) {
            $transaction = Transaction::find($id);
            $amount = (float) str_replace(',', '', $request->amount);

            $transaction->amount = $amount;
            $transaction->notes = $request->notes;
            $transaction->updated_at = new DateTime();
            $transaction->save();
            return redirect('transaction')->with('status', 'data sudah berhasil di update');
        }
        return view('dashboard');
    }

    // delete transaction
    public function delete(Request $request, $id)
    {
        $transaction = transaction::find($id);
        $transaction->delete();
        return redirect('transaction')->with('status', 'data has been deleted');
    }
}
