<?php

namespace App\DataTables;

use App\Models\transaction;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class transactionsDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('actions', function ($row) {
                return "<a href='/transaction/edit/" . $row->id . "' class='btn btn-xs btn-info pull-right'>Edit</a>";
            })
            ->rawColumns(['actions'])
            ->editColumn('amount', function ($transaction) {
                return number_format($transaction->amount, 2, ',', '.');
            })
            ->editColumn('created_at', function ($transaction) {
                return $transaction->created_at->format('d-m-Y h:i');
            })
            ->editColumn('updated_at', function ($transaction) {
                return $transaction->updated_at->format('d-m-Y h:i');
            })
            ->setRowId('id');
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(transaction $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('transactions-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->orderBy(1)
            ->selectStyleSingle()
            ->buttons([
                Button::make('excel'),
                Button::make('csv'),
                Button::make('pdf'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            ]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::computed('actions')
                ->exportable(true)
                ->printable(true)
                ->width(60)
                ->addClass('text-center'),
            Column::make('id'),
            Column::make('code'),
            Column::make('transaction_type'),
            Column::make('amount'),
            Column::make('notes'),
            Column::make('created_at'),
            Column::make('updated_at'),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'transactions_' . date('YmdHis');
    }
}
