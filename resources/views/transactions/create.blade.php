@extends('layouts.app')

@section('style')
    <link href='https://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
@endsection

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Create transaction</div>
            <div class="card-body">
                <ul id="errors"></ul>

                <form name="create-transaction" id="createTransaction" method="post" enctype="multipart/form-data"
                    action="{{ url('transaction/store') }}">
                    @csrf
                    <div class="form-group">
                        <label>Transaction Type</label>
                        <select name="transaction_type" id="transactionType" class="form-control">
                            <option value="D">TOP UP</option>
                            <option value="C">TRANSACTION</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Code</label>
                        <input type="text" id="code" name="code" class="form-control" readonly value="TOPUP - ">
                    </div>

                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" name="amount" id="amount" class="form-control"
                            required="amount is required"></textarea>
                    </div>


                    <div class="upload">
                        <div class="form-group">
                            <label>Bukti Topup</label>
                            <input type="file" name="file" id="file-upload" class="form-control" accept="image/*">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Notes</label>
                        <textarea name="notes" class="form-control" required="notes is required"></textarea>
                    </div>
                    <button type="submit" id="submitForm" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    </script>

    <script>
        $(document).ready(function() {
            $("#amount").numberFormatter({
                thousandSeparator: '.',
                decimalSeparator: ',',
            });

            $("#transactionType").on("change", function() {
                var transactionType = $(this).val();
                if (transactionType == "D") {
                    $(".upload").css("display", "");
                    $("#code").val("TOPUP - ");
                } else {
                    $(".upload").css("display", "none");
                    $("#file-upload").val("");
                    $("#code").val("TRANSACTION - ");
                }
            });
        });
    </script>
@endpush
