@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{ route('create-transaction') }}" class="btn btn-info" role="button">New</a>
        <br />
        <br />
        <div class="card">
            <div class="card-header">Manage Transaction</div>
            <div class="card-body">
                {{ $dataTable->table() }}
            </div>
            <div class="card-footer">
                Total Amount : {{ number_format($amount, 2) }}
                <br />
                <a href="{{ route('dashboard') }}" class="btn btn-info" role="button">Back</a>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
@endpush
